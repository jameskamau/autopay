# import requests
# from requests.auth import HTTPBasicAuth

#from access_token import generate_access_token
from rabbitMQPublisher import publish
from databaseHandler import *
import json
from datetime import datetime
import pika
import pytz
# from databaseHandler import getMemberNumber
# from databaseHandler import getMemberNumber
# import keys

def getTime(TransTime):
    transaction_datetime = datetime.strptime(TransTime, "%Y%m%d%H%M%S")
    transdatetime = pytz.utc.localize(transaction_datetime)
    return transaction_datetime.date()

# def register_url():
#
#     #my_access_token = generate_access_token()
#
#     api_url = "https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl"
#
#     headers = {"Authorization": "Bearer %s" % my_access_token}
#
#     # request = {
#     #     "ShortCode": keys.shortcode,
#     #     "ResponseType": "Completed",
#     #     "ConfirmationURL": "http://api.zimele.co.ke/payments/api/mobile/confirmation",
#     #     "ValidationURL":   "http://api.zimele.co.ke/payments/api/mobile/validation",
#     # }
#
#     request = {
#         "ShortCode": keys.shortcode,
#         "ResponseType": "Completed",
#         "ConfirmationURL": "https://sokachamp.com/api/confirmation",
#         "ValidationURL":   "https://sokachamp.com/api/validation",
#     }
#     print(request)
#     try:
#         response = requests.post(api_url, json=request, headers=headers)
#     except:
#         response = requests.post(api_url, json=request, headers=headers, verify=False)
#
#     print(response.text)
#
#
#
#
# def simulate_c2b_transaction():
#     my_access_token = generate_access_token()
#
#     api_url = "https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate"
#
#     headers = {"Authorization": "Bearer %s" % my_access_token}
#
#     request = {
#         "ShortCode": keys.shortcode,
#         "CommandID": "CustomerPayBillOnline",
#         "Amount": "10",
#         "Msisdn": keys.test_msisdn,
#         "BillRefNumber": "000004565",
#     }
#     try:
#         response = requests.post(api_url, json=request, headers=headers)
#
#     except:
#         response = requests.post(api_url, json=request, headers=headers, verify=False)
#
#     print(response.text)

Queue='M-PESA_PENSION_QUEUE'
Exchange='M-PESA_PENSION_EXCHANGE'
Routingkey='mpesa501100'


def publish_message():
    message= {
        "TransactionType":"Pay Bill ",
        "TransID":"OGK7WB5N0L",
        "TransTime":"20200720145344",
        "TransAmount":"50.00",
        "BusinessShortCode":"501100",
        "BillRefNumber":"561235",
        "InvoiceNumber":"0",
        "OrgAccountBalance":"90800.00",
        "ThirdPartyTransID":"0",
        "MSISDN":"254720991833",
        "FirstName":"FRANCIS",
        "MiddleName":"KIPKIRUI",
        "LastName":"SANG"
      }
    # message= {
    #     "TransactionType":"Pay Bill ",
    #     "TransID":"QQFT14SJ2J",
    #     "TransTime":"20200724163107",
    #     "TransAmount":"13335.00",
    #     "BusinessShortCode":"501100",
    #     "BillRefNumber":"65468521",
    #     "InvoiceNumber":"0",
    #     "OrgAccountBalance":"90800.00",
    #     "ThirdPartyTransID":"0",
    #     "MSISDN":"254720392815",
    #     "FirstName":"EDWIN",
    #     "MiddleName":"OKELLO",
    #     "LastName":"WASICHE"
    #   }

    # PORTFOLIO,ACCOUNT_NO =getAccountType("000000329")
    #
    # print("from C2B :"+PORTFOLIO)
    # print("from C2B :"+ACCOUNT_NO)
    getMemberNumber(message,getTime(message["TransTime"]),message["FirstName"],message["MSISDN"],message["BillRefNumber"],message["BillRefNumber"])
    # fomartMemberNumber(message["BillRefNumber"])
    #publish(message,Exchange,Queue,Routingkey)

publish_message()
#register_url()
#simulate_c2b_transaction()
