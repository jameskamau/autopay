import json
import logging
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
from Crypto.Cipher import AES
from Crypto import Random
import base64

def decryption():

    # message = json.loads(request.data)
    with open('/home/zimeledigital/data.json') as f:
        message = json.load(f)
    logging.info("Message: {}".format(message))

    key = message.get('key')
    key = base64.b64decode(key)
    # private_key_string = open('/home/zimeledigital/key/scb-apibanking-client-cert-private-key.pem', "rb").read()
    #
    private_key_string = open('/home/zimeledigital/key/scb-apibanking-client-cert-private-key.pem').read()
    private_key = RSA.importKey(private_key_string, passphrase='123456')
    verifier = PKCS1_v1_5.new(private_key)

    sentinel = Random.new().read(32)
    key_to_content = verifier.decrypt(key, sentinel)

    content = message.get('content')
    content = base64.b64decode(content)
    if len(content) <= AES.block_size:
        logging.info("Message: {}".format("Invalid ciphertext."))
        raise Exception("Invalid ciphertext.")

    iv = content[:16]

    cipher = AES.new(key_to_content, AES.MODE_CBC, iv)
    data = unpad(cipher.decrypt(content[16:]))

    front_cipher = AES.new(key_to_content, AES.MODE_ECB)
    front_string = front_cipher.decrypt(content[:16])

    decrypted_content = front_string + data
    logging.info("Message: {}".format(decrypted_content))
    print(decrypted_content)

    return decrypted_content
